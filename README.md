# Avenue  Code

## Getting started:

Automation project for avenue code i use Gherkin syntax for especification example, Java language, Cucumber to create integration with methods with gherkin syntax, Selenium framework, i used PageFactory to support WebElements using (css selector, xpath, id etc...)

### Prerequisites variable enviroment:

```Java HOME v.8```
```MAVEN HOME v.3.6```

Default execution is for Mac, but you can run in Windows using parameters of maven:

1. IE_WINDOWS = Internet Explorer OS Windows
2. MOZILLA_WINDOWS = Mozilla FireFox OS Windows
3. CHROME_WINDOWS = Chrome for OS Windows
4. EDGE_WINDOWS = Edge for OS Windows
5. CHROME_MAC = Chorme for OS Mac

```Example: 'mvn clean -Dbrowser=CHROME_WINDOWS -Dtest=CucumberRunnerTest test' to execute on Windows```

To run a test unit test:

```mvn clean test -Dcucumber.options"--tags @login"```

To run a test configured in CucumberRunnerTest:

```mvn clean -Dtest=cucumberRunnerTeste test```

### Project structure tree:
```
.
├── README.md
├── data
│   ├── login.json
│   └── login_error.json
├── drivers
│   ├── linux
│   │   └── chromedriver
│   ├── mac
│   │   └── chromedriver
│   └── windows
│       ├── IEDriverServer.exe
│       ├── MicrosoftWebDriver.exe
│       ├── chromedriver.exe
│       └── geckodriver.exe
├── evidence
├── pom.xml
└── src
    └── test
        ├── java
        │   └── br
        │       └── com
        │           └── avenue
        │               ├── enums
        │               │   ├── Browsers.java
        │               │   ├── OperationSystems.java
        │               │   └── Users.java
        │               ├── model
        │               │   └── Massa.java
        │               ├── pages
        │               │   ├── LoginPage.java
        │               │   └── TasksPage.java
        │               ├── runner
        │               │   └── CucumberRunnerTest.java
        │               ├── test
        │               │   └── steps
        │               │       └── definitions
        │               │           ├── Hooks.java
        │               │           └── login
        │               └── utils                   
        │                   ├── DriverFactory.java
        │                   ├── HandleProperties.java
        │                   ├── Page.java
        │                   ├── Print.java
        │                   └── Utils.java
        └── resources
            ├── config.properties
            └── features
                ├── login.feature
                └── tasks.feature

```
### How works? 

![](gif/runner.gif)
