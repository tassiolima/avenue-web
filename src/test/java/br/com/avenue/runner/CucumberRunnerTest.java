package br.com.avenue.runner;

import br.com.avenue.enums.Browsers;
import cucumber.api.SnippetType;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, snippets = SnippetType.CAMELCASE, features = "src/test/resources/", 
glue = "br/com/avenue/test/steps/definitions", tags = {
		"@login", "~@notImplemented", "~@unit" })

public class CucumberRunnerTest {

	@BeforeClass
	public static void tearUp() {
		Browsers.setWebDriver();
	}

}
