package br.com.avenue.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import br.com.avenue.utils.Page;

public class TasksPage extends Page {

    public TasksPage() {
        PageFactory.initElements((new AjaxElementLocatorFactory(getDriver(), 15)) , this);
    }

    @FindBy(xpath = "//a[text()='My Tasks']")
    private WebElement tasks;

    @FindBy(xpath = "//span[@ng-click='addTask()']")
    private WebElement newTask;

    @FindBy(xpath = "//a[@onaftersave='saveTask(task)']")
    private WebElement editTask;

    @FindBy(xpath = "//input[@ng-model='$data']")
    private WebElement inputTask;

    @FindBy(xpath = "//span[@class='glyphicon glyphicon-ok']")
    private WebElement confirmTask;

    @FindBy(xpath = "//button[@ng-click='$form.$cancel()']")
    private WebElement cancelTask;

    @FindBy(xpath = "//div[@class='editable-error help-block ng-binding']")
    private WebElement validSuccessTask;

    @FindBy(xpath = "//tr[@class='ng-scope']")
    private WebElement removeTask;

    @FindBy(id = "new_task")
    private WebElement editTaskEnter;

    @FindBy(xpath = "//td[@class='task_body col-md-7']")
    private WebElement validTaskEnter;

    public void clickTask() {
        this.tasks.click();
    }

    public void newTask(){
        this.newTask.click();
    }

    public void editTask(String task){
        
        this.editTask.click();
        this.inputTask.sendKeys(task);
        this.confirmTask.click();
    }

    public void cancelTask() {
      
       try {
        Thread.sleep(1000);
       } catch (Exception e) {
           
       }
        this.cancelTask.click();
    }

    public boolean validTasksSuccess(){
        waitElement(By.xpath("//div[@class='editable-error help-block ng-binding']"));
        return "TaskCreated".equals(validSuccessTask.getText());
    }

    public void removeTask() {
        this.removeTask.click();
    }

    public boolean validList(){
      return  removeTask.isDisplayed();
    }

    public void editTaskEnter(String task){
        this.editTaskEnter.click();
        this.editTaskEnter.sendKeys(task);
        try {
            Thread.sleep(1000);
           } catch (Exception e) {
               
           }
    }

    public void enterTask(){
        this.editTaskEnter.sendKeys(Keys.ENTER);
    }

    public String validTask(){
        return validTaskEnter.getText();
    }

}