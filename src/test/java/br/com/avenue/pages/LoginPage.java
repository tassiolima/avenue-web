package br.com.avenue.pages;

import br.com.avenue.enums.Users;
import br.com.avenue.model.Massa;
import br.com.avenue.utils.HandleProperties;
import br.com.avenue.utils.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends Page {

	public LoginPage(){
		PageFactory.initElements(getDriver(), this);
	}

	public static Massa userLogged;

	@FindBy(xpath = "//a[text()='Sign In']")
	private WebElement signIn;

	@FindBy(css = "input[id=user_email]")
	private WebElement email;

	@FindBy(css = "input[id=user_password]")
	private WebElement password;

	@FindBy(css = "input[type=submit]")
	private WebElement confirm;

	@FindBy(css = "div[class='alert alert-info']")
	private WebElement signedAndSignout;

	@FindBy(css = "div[class='alert alert-warning']")
	private WebElement invalidSigned;

	@FindBy(xpath = "//li/a[@data-method='delete']")
	private WebElement signout;

	@FindBy(xpath = "/html/body/div[1]/h1")
	private WebElement textLogin;

	public void visitLogin() {
		String urlDevLogin = System.getProperty("urlLogin") != null ? System.getProperty("urlLogin")
				: HandleProperties.getValue("urlLogin");
		openUrl(urlDevLogin);
	}

	public void login(String user) {
		waitElement(By.xpath("//div[@class='jumbotron grey-jumbotron']"));
		userLogged = Users.valueOf(user).getMassa();
		signIn.click();
		email.sendKeys(userLogged.getUser());
		password.sendKeys(userLogged.getPassword());
		confirm.click();
	}

	public boolean validSignedAndSignout() {
		return signedAndSignout.isDisplayed();
	}

	public boolean invalidSigned() {
		return invalidSigned.isDisplayed();
	}

	public void clickSingout() {
		signout.click();
	}

	public boolean validLogin(String message) {
		return message.contains(textLogin.getText());
	}

}
