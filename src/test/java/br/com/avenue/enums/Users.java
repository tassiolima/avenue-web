package br.com.avenue.enums;

import br.com.avenue.model.Massa;
import br.com.avenue.utils.Utils;

public enum Users {

    TASSIO("data/login.json"),
    TASSIO_ERROR("data/login_error.json");

    private String jsonPath;

    Users(String jsonPath){

        this.jsonPath = jsonPath;
    }

    public Massa getMassa(){
        return Utils.pathToUser(this.jsonPath);
    }
}
