package br.com.avenue.test.steps.definitions;

import br.com.avenue.enums.Browsers;

import cucumber.api.Scenario;
import cucumber.api.java.After;

public class Hooks {

    @After
    public void afterEachScenario(Scenario scenario) throws Exception {
      //Print.takeScreenShot(scenario);
      Browsers.quitDriver();
    }
}