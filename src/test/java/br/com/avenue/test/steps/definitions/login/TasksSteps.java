package br.com.avenue.test.steps.definitions.login;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import br.com.avenue.pages.TasksPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TasksSteps {

    private TasksPage tasksPage;

    public TasksSteps() {
        tasksPage = new TasksPage();
    }

    // ----------------------------------- GIVEN -----------------------------------//


    // ----------------------------------- WHEN -----------------------------------//


    @When("^user create a task with name \"([^\"]*)\"$")
    public void userCreateATaskWithName(String task) {
        this.tasksPage.clickTask();
        this.tasksPage.newTask();
        this.tasksPage.editTask(task);
    }

    @When("^remove the same task$")
    public void removeTheSameTask() {
        this.tasksPage.cancelTask();
        this.tasksPage.removeTask();
    }

    @When("^user selected type new task \"([^\"]*)\"$")
    public void userSelectedTypeNewTask(String task)   {
        this.tasksPage.clickTask();
        this.tasksPage.editTaskEnter(task);
    }


    @When("^press ENTER$")
    public void press()   {
        this.tasksPage.enterTask();
    }

  

    // ----------------------------------- THEN -----------------------------------//
 
    @Then("^task created success$")
    public void taskCreatedSuccess() {
        assertTrue("Error for create task", this.tasksPage.validTasksSuccess());
    }

    @Then("^task is removed$")
    public void taskIsRemoved() {
        assertFalse("Tasks not found", this.tasksPage.validList());
    }
    
    @Then("^create a new task \"([^\"]*)\"$")
    public void createANewTask(String task) {
        assertTrue("Failure to create task", task.equals(this.tasksPage.validTask()));
    }
 



}