package br.com.avenue.test.steps.definitions.login;

import static org.junit.Assert.assertTrue;

import br.com.avenue.pages.LoginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {

    private LoginPage loginPage;

    public LoginSteps(){
        loginPage = new LoginPage();

    }

    // ----------------------------------- GIVEN -----------------------------------//

    @Given("^open avenue web$")
    public void openAvenueWeb() {
        this.loginPage.visitLogin();
    }

    // ----------------------------------- WHEN -----------------------------------//

    @When("^i'm logged in with user \"([^\"]*)\"$")
    public void iMLoggedInWithUser(String user) {
        this.loginPage.login(user);
    }

    @When("^go to signout$")
    public void goToSignout() {
        this.loginPage.clickSingout();
    }

    // ----------------------------------- THEN -----------------------------------//

    @Then("^sucess signed$")
    public void sucessSigned() {
        assertTrue("Invalid return text Login", this.loginPage.validSignedAndSignout());
    }

    @Then("^sucess signout$")
    public void sucessSignout() {
        assertTrue("Invalid return text Logout", this.loginPage.validSignedAndSignout());
    }

    @Then("^unsucess signed$")
    public void unsucessSigned() {
        assertTrue("Valid return Login", this.loginPage.invalidSigned());
    }

}
