package br.com.avenue.utils;

import java.util.List;

import br.com.avenue.enums.Browsers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Page {
	
private static final long DEFAULT_TIME_WAIT = 30;

	public Page() {
		if (Browsers.webDriver == null) {
			Browsers.setWebDriver();
		}
	}
	
	protected void openUrl(String url) {
		getDriver().get(url);
	}
	
	public static WebDriver getDriver() {
		return Browsers.webDriver;
	}

	protected WebElement waitVisibleElement(WebElement element){
		return new WebDriverWait(getDriver(), DEFAULT_TIME_WAIT) .until(ExpectedConditions.visibilityOf(element));
	}
	
	protected WebElement waitElement(By locator) {
		return new WebDriverWait(getDriver(), DEFAULT_TIME_WAIT)
				.until(ExpectedConditions.presenceOfElementLocated(locator));
	}
	
	protected List<WebElement> waitElements(By locator) {
		return new WebDriverWait(getDriver(), DEFAULT_TIME_WAIT)
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

}