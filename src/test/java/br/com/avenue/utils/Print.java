package br.com.avenue.utils;

import cucumber.api.Scenario;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Print {
	
	public static void takeScreenShot(Scenario scenario) {

        File file = new File("./evidence");

        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory structure got created");
            } else {
                System.out.println("Directory structure not created");
            }
        }


        Screenshot screenshot = new AShot()
        		.shootingStrategy(ShootingStrategies.viewportPasting(1000))
        		.takeScreenshot(Page.getDriver());

        try {

            ImageIO
            .write(screenshot.getImage(),"PNG",new File(file, scenario.getName() + " - " + scenario.getStatus().toUpperCase() + ".png"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
