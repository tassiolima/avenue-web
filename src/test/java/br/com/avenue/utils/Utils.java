package br.com.avenue.utils;

import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Type;


import br.com.avenue.model.Massa;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Utils {

    public static Massa pathToUser(String path) {
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<Massa>(){}.getType();
            return gson.fromJson(new FileReader(new File(path).getAbsolutePath()), type);
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }


}
