#language: en

@tasks
Feature: Create Tasks,
    Me while user,
    Want to create my tasks in application,
    For my tasks management

    Background:
        Given open avenue web
        When i'm logged in with user "TASSIO"

    @createTask
    Scenario: Create a task in my tasks
        When user create a task with name "Task123"
        Then task created success

    @removeTask
    Scenario: User remove a task in my tasks
        When user create a task with name "Task123"
        And remove the same task
        Then task is removed   

    @enterTask
    Scenario: Create a task using enter
      When user selected type new task "Task123"
      And press ENTER   
      Then create a new task "Task123"
