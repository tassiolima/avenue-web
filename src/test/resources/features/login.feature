#language: en

@login
Feature: Login,
	Me while user,
	Want to log in application,
	For my management

	Background:
		Given open avenue web

	@loginSuccess
	Scenario: Login success with user
		When i'm logged in with user "TASSIO"
		Then sucess signed

	@loginUnsucces
	Scenario: Login failure with user
		When i'm logged in with user "TASSIO_ERROR"
		Then unsucess signed 

	@signout
	Scenario: Signout with user
		When i'm logged in with user "TASSIO"
		And go to signout
		Then sucess signout

